package com.dev4abyss.movies.module.user.service;

import com.dev4abyss.movies.module.user.entity.User;
import org.springframework.stereotype.Service;

@Service
public class UserService {


    public User findByID(Long id){
        User user1 = new User();
        user1.setName("Jobs");
        user1.setEmail("Jobs@gmail.com");
        user1.setPassword("123456");

        User user2 = new User();
        user2.setName("Marcio");
        user2.setEmail("lobo@gmail.com");
        user2.setPassword("654321");

        if(id==1) {
            return user1;
        }else if(id==2){
            return user2;
            }
        else{
            throw new RuntimeException("user not found by id:" +id);
        }
    }

}
