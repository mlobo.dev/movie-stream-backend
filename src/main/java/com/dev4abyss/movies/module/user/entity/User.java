package com.dev4abyss.movies.module.user.entity;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class User {

    private String name;
    private String email;
    private String password;



}
