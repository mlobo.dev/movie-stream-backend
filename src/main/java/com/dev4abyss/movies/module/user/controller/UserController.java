package com.dev4abyss.movies.module.user.controller;

import com.dev4abyss.movies.module.user.entity.User;
import com.dev4abyss.movies.module.user.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
public class UserController {


    private final UserService service;

    @GetMapping("/user/{id}")
    public User findUserByid(@PathVariable Long id){
        return service.findByID(id);
    }
}
